package specs;

import fr.epsi.services.QuestionWS;
import fr.epsi.tools.HibernateUtil;
import org.concordion.integration.junit4.ConcordionRunner;
import org.hibernate.Session;
import org.junit.runner.RunWith;

import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;

/**
 * Created by fredericradigoy on 17/04/15.
 */
@RunWith(ConcordionRunner.class)
public class ServiceFixture {

    private QuestionWS questionWs;


    public ServiceFixture()
    {
        questionWs = new QuestionWS();
    }


    public void clean()
    {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        session.createSQLQuery("truncate table question").executeUpdate();
        session.getTransaction().commit();
    }

    public Response poserQuestionVerifierSucces(String question) throws Exception{

        Response response = null;
        try
        {
            response = questionWs.addQuestion(question);
        }
        finally
        {
            clean();
        }
        return response;
    }

    public Response getQuestionExisteSucces()
    {
        Response response = null;
        Request request = null;
        try
        {
           response = questionWs.addQuestion("Quel est le meilleur joueur de tout les temps de basketBall?");
            response = questionWs.getReponse(1, request);
        } catch (Exception e) {
            e.printStackTrace();
        } finally
        {
            clean();
        }

        return response;
    }

    public Response clientDemandeReponse() throws Exception {
        Response response = null;
        Request request = null;
        try
        {
            response = questionWs.addQuestion("qui est tu ?");
            response = questionWs.getReponse(1, "dofre", request);
            response = questionWs.getReponse(1, request);
        }
        finally {
            clean();
        }

        return response;
    }




}

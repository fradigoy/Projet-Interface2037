package fr.epsi.entites;

import fr.epsi.InvalidQuestionException;
import fr.epsi.tools.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.*;
import java.util.Objects;

/**
 * Created by fredericradigoy on 16/04/15.
 */
@Entity
@Table(name = "question")
public class Question {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Basic
    private String question;

    @Basic
    private String reponse;



    public Question()
    {

    }

    public Question(String question)
    {
        this.question = question;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getReponse() {
        return reponse;
    }

    public void setReponse(String reponse) {
        this.reponse = reponse;
    }

    public void Validate () throws InvalidQuestionException {
        if(Objects.toString(question, "").isEmpty())
            throw new InvalidQuestionException("la question posée est invalide !!");
    }

    /**
     * Cette methode permet d'enregister un question dans la base de donnee
     */

    public void save(String question)
    {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(question);
        transaction.commit();
    }



    public static Question findQuestionById(int id) {

        Question question = null;

        try
        {
            Session session = HibernateUtil.getSessionFactory().getCurrentSession();
            Transaction transaction = session.beginTransaction();
            question = (Question) session.get(Question.class, id);
            transaction.commit();

        }
        catch (Exception e)
        {

            e.printStackTrace();

        }

        return question;

    }

    public void update(String uneReponse)
    {

        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        session.update(uneReponse);
        session.getTransaction().commit();

    }


}
